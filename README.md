# COMP424 Final Project
I had to implement the logic that was going to be used by a 
computer player of the Halma game. I remember having used a simple
technique in which the utility function analyzed the move as a good 
one if the pieces were closer to the desired final position of the game.

This was written in Java.
