%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[12pt]{article}

\usepackage{fancyhdr} % Required for custom headers
\usepackage{lastpage} % Required to determine the last page for the footer
\usepackage{extramarks} % Required for headers and footers
\usepackage{graphicx} % Required to insert images
\usepackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template
\usepackage{enumerate}
\usepackage{listings}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bussproofs}
\usepackage[]{mcode}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Octave,                 % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}
% Margins
\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in 

\linespread{1.1} % Line spacing

% Set up the header and footer
\pagestyle{fancy}
\lhead{\hmwkAuthorName} % Top left header
\chead{\hmwkClass\ \hmwkTitle} % Top center header
\rhead{\firstxmark} % Top right header
\lfoot{\lastxmark} % Bottom left footer
\cfoot{} % Bottom center footer
\rfoot{Page\ \thepage\ of\ \pageref{LastPage}} % Bottom right footer
\renewcommand\headrulewidth{0.4pt} % Size of the header rule
\renewcommand\footrulewidth{0.4pt} % Size of the footer rule

\setlength\parindent{0pt} % Removes all indentation from paragraphs

%----------------------------------------------------------------------------------------
%	DOCUMENT STRUCTURE COMMANDS
%	Skip this unless you know what you're doing
%----------------------------------------------------------------------------------------

% Header and footer for when a page split occurs within a problem environment
\newcommand{\enterProblemHeader}[1]{
\nobreak\extramarks{#1}{#1 continued on next page\ldots}\nobreak
\nobreak\extramarks{#1 (continued)}{#1 continued on next page\ldots}\nobreak
}

% Header and footer for when a page split occurs between problem environments
\newcommand{\exitProblemHeader}[1]{
\nobreak\extramarks{#1 (continued)}{#1 continued on next page\ldots}\nobreak
\nobreak\extramarks{#1}{}\nobreak
}

\setcounter{secnumdepth}{0} % Removes default section numbers
\newcounter{homeworkProblemCounter} % Creates a counter to keep track of the number of problems

\newcommand{\homeworkProblemName}{}
\newenvironment{homeworkProblem}[1][Problem \arabic{homeworkProblemCounter}]{ % Makes a new environment called homeworkProblem which takes 1 argument (custom name) but the default is "Problem #"
\stepcounter{homeworkProblemCounter} % Increase counter for number of problems
\renewcommand{\homeworkProblemName}{#1} % Assign \homeworkProblemName the name of the problem
\section{\homeworkProblemName} % Make a section in the document with the custom problem count
\enterProblemHeader{\homeworkProblemName} % Header and footer within the environment
}{
\exitProblemHeader{\homeworkProblemName} % Header and footer after the environment
}

\newcommand{\problemAnswer}[1]{ % Defines the problem answer command with the content as the only argument
\noindent\framebox[\columnwidth][c]{\begin{minipage}{0.98\columnwidth}#1\end{minipage}} % Makes the box around the problem answer and puts the content inside
}

\newcommand{\homeworkSectionName}{}
\newenvironment{homeworkSection}[1]{ % New environment for sections within homework problems, takes 1 argument - the name of the section
\renewcommand{\homeworkSectionName}{#1} % Assign \homeworkSectionName to the name of the section from the environment argument
\subsection{\homeworkSectionName} % Make a subsection with the custom name of the subsection
\enterProblemHeader{\homeworkProblemName\ [\homeworkSectionName]} % Header and footer within the environment
}{
\enterProblemHeader{\homeworkProblemName} % Header and footer after the environment
}
   
%----------------------------------------------------------------------------------------
%	NAME AND CLASS SECTION
%----------------------------------------------------------------------------------------

\newcommand{\hmwkTitle}{Final\ Project} % Assignment title
\newcommand{\hmwkDueDate}{Friday,\ April\ 11,\ 2014} % Due date
\newcommand{\hmwkClass}{COMP\ 424} % Course/class
\newcommand{\hmwkClassTime}{11:59pm} % Class/lecture time
\newcommand{\hmwkClassInstructor}{Joelle Pineau} % Teacher/lecturer
\newcommand{\hmwkAuthorName}{Ataias Reis/260590875} % Your name

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\title{
\vspace{2in}
\textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
\normalsize\vspace{0.1in}\small{Due\ on\ \hmwkDueDate}\\
Student: Ataias Pereira Reis ID: 260590875\\
\vspace{0.1in}\large{\textit{\hmwkClassInstructor\ \hmwkClassTime}}
\vspace{2in}
}

\author{\textbf{\hmwkAuthorName}}
\date{} % Insert date here if you want it to appear below your name

%----------------------------------------------------------------------------------------

\begin{document}

\section{Overview of s260590875 AI agent}
\paragraph{} This AI agent does the following:
\begin{itemize}
\item Receive a request to make a move, together with a board object
\item Analyzes of whom is the turn when its function is called
\item Requests all the possible movements (called here root movements)
  \begin{itemize}
    \item For each root movement that doesn't reduce the utility by much, compute the ``Sequence Utility''. This is the maximum utility that is possible to achieve in two movements after the first one was done and then return this utility.
  \end{itemize}
\item For the root movement with the bigger sequence utility, consider it as the bestMove
\item If for some reason this movement nullifies the effect of a previous movement (moved from (1,1) to (2,2) and now from (2,2) to (1,1)), a loop is happening. This usually happens close to the goal state. To get out of this problem, choose a random move and then return it
\item If no problem was found, like a loop, then return the bestMove that was found by search
\end{itemize}
\paragraph{} These lines above will be better explained in the next paragraphs.
\paragraph{} The agent, therefore, tries to maximize its utility by doing a search with depth-3 (depth-limited search, limited to 3 levels) in the space of movements in order to find the best one. The basic idea was delineated above, now notice that to analyze a sequence of movements, after a movement that is not a hop, one would need to consider the movement of the other three players. This case is simplified by considering random moves for the other players. This means their movements are disregarded and only the own player moves are searched. For such a search, only a Java recursive stack is used (recursive function stack), no tree data structures or anything similar. 
\paragraph{} Regarding the ``Sequency Utility'', pay heed that in the case it chooses a non-hop movement that doesn't look optimal but enables a movement that will make the utility increase more than other movements, the movement that precisely would make the utility increase is not saved. As the other players can do basically anything and this is not considered, the searched movements for the next turns are merely discarded. The search repeats again in the next turn, and looks for the best movement, that can be a different one from what was thought previously.
\paragraph{} Each player has 13 pieces, and the utility is computed solely on the position of those 13 pieces, without considering pieces of other players. The AI move choice also has an extra consideration: hop movements are worth more. This means that, for example, if taking one hop movement or taking two simple movements give the same utility, the hop movement is usually preferred. Hop movements in only one turn can ably move several positions. Advantaging those moves is not automatic with the utility function (to be defined soon), as it has no notion of time. 
\paragraph{} In relation to random moves, they are used more frequently close to the goal because that are times that pieces arrive in one corner and get stuck there, out of the goal, because of pieces already occupying the closest goal position. In the other corner there would be a spot that needs to be filled and the search can't see that because it is only depth-3 search. To solve this, I simply stop the depth-3 search and use random movements and greedy search, based only on the next best step. This works but may take a few movements and the other player may have more time to win. In this case, the agent is really taking a chance.
\paragraph{} The motivation for a depth-3 search was to analyse movements with their consequences, and to find the ones that seemed better, even though under uncertainty regarding the movement of other players. If not doing it, only the current possible movements would be considered and this is usually far from optimal. This would be a pure greedy search. If a depth-4 search was used, it would consume too much time, so that's why it is only doing a depth 3 search. Even for a depth-3 search, a few times it takes time more than 1s. To solve that, the player analyzes the system time and stops searching at around 0.9s and returns the best move so far found.
\paragraph{} Regarding the utility function, it takes all the position $P_k=(i,j)$, $k=1\ldots 13$ and $i,j=1\ldots 16$, of the pieces of the player and sums the values obtained from the utility matrix $M$ to get the function value for the board, therefore:
\begin{eqnarray*}
U(\textrm{Board}_x)=U(P_1,\ldots,P_{13})=\sum_{k=1}^{13}M(P_k)
\end{eqnarray*}
\paragraph{} The utility matrix $M$ is defined in order to move the players from their corners to their respective goals without veering much to the sides. A miniature 8x8, instead of 16x16, is presented below for player 0:
\begin{eqnarray*}
M=\left[\begin{array}{cccccccc}
0 &	12 &	24 &	 36	& -96 &	-120 & 	-144 &	-168\\
12 &	24 &	36 & 	48 &	 60 & 	-144 &	-168 &	-192\\
24 & 36 & 48 &	60 &	 72 &	84 & 	-192 &	-216\\
36 & 48 &	60 &	 72  &	84 & 	96 &	108 &	-240\\
-96	& 60 &	72 &	 84 &	 96 &	108 &	120 &	132\\
-120 &	-144 &	84 &	 96 &	108 &	120 &	136 &	104\\
-144 &	-168 &	-192 &	108 &	120 &	134 &	144 &	156\\
-168 &	-192 &	-216 &	-240 &	132 &	104 &	156 &	168\\
\end{array}\right]
\end{eqnarray*}
\paragraph{} It is noticeable that if pieces move along the diagonals with positive numbers, getting closer to the corner (8,8) makes the utility, as defined, increase. This matrix is almost symmetric. Close to the goal there are a few numbers that were changed manually to break the symmetry. The idea is to make pieces that arrive in one of the corners to move along this diagonal if inner positions are already occupied. This helps to avoid certain loops, but not all.
\paragraph{} The side diagonals, with negative numbers, present terrible positions for the pieces to be (at least as defined by the utility function), so they will avoid it because of the lower utility. For the other players, the matrix $M_0$ for player 0 is basically rotated. The matrices for players 0, 1, 2 and 3 would be:
\begin{eqnarray*}
M_0&=&\textrm{coded similar to M of example but 16x16}\\
M_1(i,j)&=&M_0(15-i,j)\\
M_2(i,j)&=&M_0(i,15-j)\\	
M_3(i,j)&=&M_0(15-i,15-j)
\end{eqnarray*}
where $i$ and $j$ and indexes from $0$ to $15$.
\paragraph{} Now, let's talk about why there is some pruning in this agent. Not every movement is worth analyzing. The reason why is that, even when considering move sequences, there are movements that are clearly bad, for example, doing a hop diagonally backwards (getting closer to the start position), is one that would not be worth considering. For this, a certain pruning is necessary such that those nodes are not analyzed further. 
\section{Theorical Basis}
\paragraph{} The idea of an utility function comes from lecture 7 - Games. There we learned that ``we want to find a strategy that maximizes utility''. The search scheme was from lecture 2 - Uninformed Search. In the algorithm here, Depth-limited search was used, with only one depth, namely of 3. This method is recommended to use when there isn't enough space, what is the case but it was used here mainly because of time. It is also simpler than a breadth-limited search.  Regarding the pruning, it is also motivated from lecture 7, where it teaches $\alpha$-$\beta$ pruning. The pruning that I use is different from this one, but it was motivated from that.
\paragraph{} The idea of preferring hops instead of simple movements came from observation of a few number of play-outs, between 20 and 50.
\paragraph{} No algorithms from other sources were used.
\section{Advantages and Disadvantages}
The advantages are 
\begin{itemize}
\item Many times in play-outs, took better movements than the ones in my mind
\item Makes reasonable improvements at each turn by making use of hops
\item Reasonably simple
\item Pruning makes search take less time by disconsidering too bad root movements
\end{itemize}
The disadvantages are:
\begin{itemize}
\item It is not guaranteed that the piece will get out of the base in 100 rounds of the player, but basically all my tests there were no problems regarding that.
\item When there is uncertainty of a good move, takes a random move and sometime it is not good
\item Doesn't make use of partner that has similar goal of getting to the opposite corner as fast as possible.
\item May do movements that help the opposite team
\item Pruning as it is done may have its downsides. It could be that one last movement was pretty bad because (maybe because of other player movements that were not random, or any other thing that is not considered) and search now found a better move but it would need to move one piece closer to origin. I didn't delve if a situation like this exists, but the question is open and so far it looks like a disadvantage or the pruning
\end{itemize}
\section{Future improvements}
\paragraph{} I had to try a couple games in order to detect that simply an utility function was not enough. This was because a manually made utility function could not consider many details of the game as I am not an expert myself in it. This made the ideas to do a search and to tweak the preference of hop movements. Therefore, the future improvement that I would consider is to use machine learning in this game. Letting the computer play itself and, just by receiving information if the result was good, discover how to play, would be amazing. I think it would take too many games for this though, and possibly better data structures would need to be used. A better search algorithm is also a good bet, I deployed Depth-Limited Search, this could have been some depth-limited A*, by adding a heuristic. The heuristic could initially be made by hand and then improved with learning.
\paragraph{} Other thing, in my code I don't consider anything the other players do. An analysis of their movements could be done to improve the player in order to hinder the enemy by analyzing the most likely movements it could make, with the help of probabilities. The player could be also analyze and help its teammate. 
\paragraph{} Random movements could be abolished. Improvements in the utility function and the search would make then not necessary. Other option would to do a different search when there is uncertainty, maybe a deeper search considering only pieces that are out of the goal could be considered. A little more depth would be achieved depending how many pieces are considered. 
\paragraph{} The titles of future improvements are presented below:
\begin{itemize}
\item A* depth limited instead of Depth-Limited Search (informed search instead of uninformed). Need to create heuristic
\item Machine Learning to improve utility function and heuristic for search
\item Use of data structure to search, instead of simply using Java stack
\item  In the data structure, order data in a way that maximizes pruning so that $\alpha$-$\beta$ pruning can be used to in its best advantage.
\item Consider the other players, the adversaries and also the teammate, in order to try to defeat one and help the other, and hopefully be helped.
\item Abolish or at least reduce random moves
\end{itemize}

%---------------------------------------------------------------------

\end{document}
