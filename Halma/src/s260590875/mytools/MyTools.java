package s260590875.mytools;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

import halma.CCBoard;
import halma.CCMove;

/**
 *A Halma player subpackage implemented by Ataias Pereira Reis.
 *Student#: 260590875
 *McGill University
 *Winter 2014
 *COMP 424
 */

public class MyTools {

	//UP -> Utility (Matrix) of Players
	static int[][][] UP;
	//Size of grid
	int SIZE=16;
	static Random rand = new Random();
	//Vector to save the last movement of each player
	static CCMove[] lastMove=null;
	static boolean lastMoveHop=false;
	//This step influences the size of numbers in the utility matrix
	//Between nearby points the difference that the utility function
	//will count depends on this number.
	private static final int step=12;
	private static final int threshold=26*step*13;
	/**MyTools() - constructor
	 * allocates memory for variables
	 * initializes variables
	 * */
	public MyTools(){
		lastMove = new CCMove[4];
		for(int i=0; i<4; i++){
			lastMove[i]=null;
		}
		
		UP = new int[4][16][16];
		for(int i=0; i<SIZE; i++){
			for(int j=0; j<SIZE; j++){
				UP[0][i][j]=step*(i+j);
			}
		}
		for(int k=5; k<SIZE; k++){
			for(int i=k; i<SIZE; i++){
				int j=i-(k-1);
				int val = -2*step*(i+j);
				UP[0][i][j]=val;
				UP[0][j][i]=val;
			}
		}
		//These two lines break the symmetry close to the goal state
		//in order to show that it is better to be in certain position than
		//others
		//will try to avoid this positions with -40 and will prefer nearby ones
		UP[0][15][11]=UP[0][15][11]-40; 
		UP[0][11][15]=UP[0][11][15]-40;
		//when it is step/6 or step/3 that is added, it will try to move diagonally 
		//from the non-added points
		UP[0][12][13]=UP[0][12][13]+step/3;
		UP[0][13][12]=UP[0][13][12]+step/6;
		//Generates other utility matrices as rotations of the main one
		for(int i=0; i<SIZE; i++){
			for(int j=0; j<SIZE; j++){
				UP[2][i][j]=UP[0][i][SIZE-1-j];
				UP[1][i][j]=UP[0][SIZE-1-i][j];
				UP[3][i][j]=UP[0][SIZE-1-i][SIZE-1-j];
			}
		}
	}
	
	/** getMyUtility(board,ID)
	 * obtains utility for a certain player ID (between 0 and 3)
	 * just takes the positions of the pieces and 
	 * */
	public static int getMyUtility(CCBoard board,int ID){
		ArrayList<Point> pieces = board.getPieces(ID);
		int utility=0;
		for(Point p : pieces){
			utility = UP[ID][p.x][p.y] + utility;
		}
		return utility;
	}
	
	/** getBestMove(board)
	 * Given a board, analyzes of whom is the turn
	 * and tries to get a good move
	 * Makes use of search and pruning
	 * */
	public static CCMove getBestMove(CCBoard board){
		ArrayList<CCMove> moves = board.getLegalMoves();
		//establishes threshold to computation to 880ms from now
		long end = System.currentTimeMillis() + 880;
		
		int utility=0;
		int ID = board.getTurn();
		int oldUtility=getMyUtility(board,ID);
		int bestUtility=oldUtility;
		
		CCMove bestMove=new CCMove(ID,null,null);
		for(CCMove m : moves){
			CCBoard b = (CCBoard) board.clone();
			b.move(m);
			int newUtility = getMyUtility(b,ID);
			//certain backward movements are not worth considering
			//if they make the utility smaller by 2 steps, then do not consider it
			//this is the pruning part
			if(newUtility <= oldUtility-step) continue;
			//Here it may choose between doing a search or just picking greedily a movement
			//if threshold is not passed yet, then most of the pieces are not in the goal area
			//so it is better to do a search and do a good movement
			//otherwise, we may have only a piece and not many sequences are available for that piece
			//that's the reason why it would act greedily.
			if(oldUtility<threshold){
				//This 1 in the line below is the depth of the search. getBestMove works
				// in depth 0, so getSequenceUtility will start at depth 1
				//SequenceUtility will analyze the best utility one can get after a certain movement
				//that is why b here, the board we are passing, is already with a modification
				utility = getSequenceUtility(b, 1, ID,newUtility,end);
				//extra value is given if movement is hop
				if(m.isHop() && utility<threshold) utility = utility + 3*step;
				if(utility > bestUtility) {
					bestUtility = utility;
					bestMove = m;
				} 	
			} else {
				if(newUtility > bestUtility){
					bestUtility = newUtility;
					bestMove = m;
				}
			}
			//if no more time... well.. just return
			if(System.currentTimeMillis()>=end) break;
		}
		
		//If there was a failure in the function to get a good move, our bestMove would be (null,null)
		//as it was initialized with that value. If the move is illegal, take a random move.
		if(!board.isLegal(bestMove)){
			bestMove = moves.get(rand.nextInt(moves.size()));
		}
		//If the last move does the reverse of the move that was obtained now, a loop is occurring,
		// just take a random move to avoid that
		if(lastMove[ID]!=null && (bestMove.getTo()!=null) && lastMove[ID].getTo()!=null){
			if((lastMove[ID].getFrom().x==bestMove.getTo().x) && (lastMove[ID].getFrom().y==bestMove.getTo().y)){
				bestMove = moves.get(rand.nextInt(moves.size()));
			}
		}
		lastMove[ID]=bestMove;
		return bestMove;
	}
	
	/** getSequenceUtility(board, depth, ID, oldUtility)
	 * searches through movements and analyzes many different
	 * utilities. It returns simply the best utility given all movements possible for
	 * player "ID" with the given board
	 * */
	private static int getSequenceUtility(CCBoard board, int depth, int ID, int oldUtility, long end){
		ArrayList<CCMove> moves = board.getLegalMoves();
		int utility=0;
		int bestUtility=oldUtility;
		while(board.getTurn()!=ID){
			//make a random move of the other player
			board.move(moves.get(rand.nextInt(moves.size())));
			//get legal moves for next player
			moves = board.getLegalMoves(); 
		}
		CCBoard b=null;
		for(CCMove m : moves){
			b = (CCBoard) board.clone();
			b.move(m);
			utility = getMyUtility(b,ID);
			//if no more time, get out of the loop
			if(System.currentTimeMillis()>=end) break;
			if(m.isHop() && utility < threshold) {
				//extra value given if movement is hop
				utility = utility + 3*step;
			}
			// do not care about moves that make utility smaller
			if(utility > bestUtility) {
				bestUtility = utility;
			}
			//certain backward movements are not worth considering
			if(utility <= oldUtility - step) continue;
			//only up to depth 2
			if(depth<2){
				utility = getSequenceUtility(b,depth+1,ID,oldUtility,end);
				if(utility > bestUtility) {
					bestUtility = utility;
				}
			}
		}
		return bestUtility;
	}
	
}
