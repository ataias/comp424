package s260590875;

import halma.CCBoard;
import halma.CCMove;

import java.util.Random;

import s260590875.mytools.MyTools;


import boardgame.Board;
import boardgame.Move;
import boardgame.Player;

/**
 *A Halma player implemented by Ataias Pereira Reis.
 *Student#: 260590875
 *McGill University
 *Winter 2014
 *COMP 424
 */
public class s260590875Player extends Player {
    Random rand = new Random();
    public static MyTools tool = new MyTools();
    /** Provide a default public constructor */
    public s260590875Player() { this("260590875"); }
    public s260590875Player(String s) { 
    	super(s); 
    }
    
    public Board createBoard() { return new CCBoard(); }

    /** Choose moves according the MyTools.getBestMove() that was implemented */
    public Move chooseMove(Board theboard) 
    {
        CCBoard board = (CCBoard) theboard;
        long start = System.currentTimeMillis( );
        CCMove bestMove = MyTools.getBestMove(board);
        long end = System.currentTimeMillis( );
        System.out.println("Spent " + (end-start) + " miliseconds");
        return bestMove;
    }
    
}
